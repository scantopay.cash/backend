const mongoose = require('mongoose')

const schema = new mongoose.Schema({
  terminalId: { type: String, unique: true },
  terminalPassword: { type: String },
  payoutAddress: String,
  currency: { type: String, default: 'USD' },
  network: { type: String, default: 'main' },
  memo: String,
  expiry: Number,
  paymentRequest: {
    amount: Number,
  },
  invoice: mongoose.Schema.Types.Mixed
}, {
  timestamps: true
})

module.exports = mongoose.model('Terminal', schema)
