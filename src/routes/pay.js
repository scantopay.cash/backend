'use strict'

const config = require('../config')
const Terminal = require('../models/terminals')

const express = require('express')
const router = express.Router()

const _ = require('lodash')
const axios = require('axios')
const CashPay = require('@developers.cash/cash-pay-server-js')

class PayRoute {
  constructor () {
    router.get('/:terminalId', (req, res, next) => this.getPay(req, res, next))

    return router
  }

  async getPay (req, res, next) {
    try {
      // Get the request parameters.
      const terminalId = req.params.terminalId

      // Make sure user isn't trying to access this url directly
      if (!req.get('accept').startsWith('application')) {
        throw new Error('Invalid "accept" header. Did you remember to add the bitcoincash:?r= prefix to the URL and URI encode the query parameters?')
      }

      // Get the terminal this Terminal ID corresponds to.
      const terminal = await Terminal.findOne({ terminalId })

      // If this terminal does not exist or a payment request is not set...
      if(!terminal || !terminal.invoice) {
        throw new Error('No payment information has been set for this terminal')
      }

      // Detect request type (BIP70 vs JPP)
      const type = req.get('accept') === 'application/bitcoincash-paymentrequest' ? 'BIP70' : 'JPP'

      // Proxy the Payment Request
      const payload = await axios.get(terminal.invoice.service.paymentURI, {
        responseType: type === 'BIP70' ? 'arraybuffer' : 'json',
        headers: {
          accept: req.headers.accept,
          'user-agent': req.headers['user-agent']
        }
      })

      // Send the response
      return res.set(payload.headers)
                .send(payload.data)
    } catch (err) {
      return next(err)
    }
  }
}

module.exports = new PayRoute()
