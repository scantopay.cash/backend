'use strict'

const express = require('express')
const router = express.Router()

class IndexRoute {
  constructor () {
    // Public
    router.get('/', (req, res, next) => res.send('ok'))
    router.use('/pay', require('./pay'))
    router.use('/terminal', require('./terminal'))

    return router
  }
}

module.exports = new IndexRoute()
