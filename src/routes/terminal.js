'use strict'

const config = require('../config')
const Terminal = require('../models/terminals')

const express = require('express')
const router = express.Router()

const _ = require('lodash')
const axios = require('axios')
const CashPay = require('@developers.cash/cash-pay-server-js')

class TerminalRoute {
  constructor () {
    router.post('/:terminalId/:terminalPassword/create-invoice', (req, res, next) => this.postCreateInvoice(req, res, next))
    router.get('/:terminalId/:terminalPassword/settings', (req, res, next) => this.getSettings(req, res, next))
    router.post('/:terminalId/:terminalPassword/settings', (req, res, next) => this.postSettings(req, res, next))

    return router
  }

  async postCreateInvoice (req, res, next) {
    try {
      // Get the request parameters.
      const terminalId = req.params.terminalId
      const terminalPassword = req.params.terminalPassword

      // Get the terminal this Terminal ID corresponds to.
      const terminal = await Terminal.findOne({ terminalId })

      // If there is no terminal return a 404 response...
      if(!terminal) {
        return res.status(404).send(`Failed to find Terminal ID (${terminalId})`)
      }

      // Ensure that the password is correct.
      if (terminalPassword !== terminal.terminalPassword) {
        return res.status(403).send(`Incorrect Password for Terminal ID (${terminalId})`)
      }

      // Get the payment amount from the body of the request.
      const amount = req.body.amount;

      // Create invoice
      const invoice = new CashPay.Invoice()
        .addAddress(terminal.payoutAddress, `${amount}${terminal.currency}`)
        .setMemo(`(${terminal.terminalId}) ${terminal.memo}`)
        .setExpires(terminal.expiry)

      // If an API Key has been set for CashPayServer, then use it.
      if (process.env.CASHPAYSERVER_APIKEY) {
        invoice.setAPIKey(process.env.CASHPAYSERVER_APIKEY)
      }

      // Create the invoice on our Cash Pay Server.
      await invoice.create()

      // Update this terminal to use store the Invoice created by Cash Pay Server.
      await terminal.update({ invoice: invoice.payload() })

      res.send(invoice.payload())
    } catch(err) {
      return next(err)
    }
  }

  async getSettings (req, res, next) {
    try {
      // Get the request parameters.
      const terminalId = req.params.terminalId
      const terminalPassword = req.params.terminalPassword

      // Get the terminal this Terminal ID corresponds to.
      const terminal = await Terminal.findOne({ terminalId })

      // If there is no terminal return a 404 response...
      if (!terminal) {
        return res.status(404).send()
      }

      // Ensure that the password is correct.
      if (terminalPassword !== terminal.terminalPassword) {
        return res.status(403).send(`Incorrect Password for Terminal ID (${terminalId})`)
      }

      // Return the terminal data.
      res.send(terminal.toJSON())
    } catch(err) {
      return next(err)
    }
  }

  async postSettings (req, res, next) {
    try {
      // Get the request parameters.
      const terminalId = req.params.terminalId
      const terminalPassword = req.params.terminalPassword

      // Ensure that the merchant ID is 32 hexadecimal characters.
      if (terminalId.length !== 8)
      {
        return res.status(400).send('Terminal ID must be 8 characters.')
      }

      // Get the terminal this Terminal ID corresponds to.
      let terminal = await Terminal.findOne({ terminalId })

      // If it already exists, throw an error.
      if (terminal) {
        return res.status(403).send('Terminal already exists. To modify settings, a new Terminal must be printed.')
      }

      // Merge the Merchant ID and Terminal ID into the payload such that they take precedence.
      const terminalParams = Object.assign({}, req.body, { terminalId, terminalPassword })

      // Get the terminal this Terminal ID corresponds to.
      terminal = await Terminal.updateOne({ terminalId }, terminalParams, { upsert: true });

      // TODO: Remove me later.
      console.log('Terminal created: ', req.body)

      // Return the updated (or created) terminal.
      res.send()
    } catch(err) {
      return next(err)
    }
  }
}

module.exports = new TerminalRoute()
